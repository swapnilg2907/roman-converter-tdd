package com.thoughtworks.vapasi;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String romanNumber) {
           return calculateArabicNumber(romanNumber);
    }

    private int calculateArabicNumber(String roman) {
        int arabicNumber = 0;
        int i = 0;
        int currentNumber = 0;
        int romanNumberLength =  roman.length();
        try {
            while (i < romanNumberLength) {

                char letter = roman.charAt(i);

                currentNumber = getRomanValueToArabic(letter);
                i++;


                if (i == romanNumberLength)
                    arabicNumber += currentNumber;
                else {

                    int nextNumber = getRomanValueToArabic(roman.charAt(i));
                    if (nextNumber > currentNumber) {
                        arabicNumber += (nextNumber - currentNumber);
                        i++;

                    } else arabicNumber += currentNumber;

                }
            }
            return arabicNumber;
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }

    private int getRomanValueToArabic(char romanDigit) {
        int arabic = 0;
        switch (romanDigit) {
            case 'I':
                arabic = 1;
                break;
            case 'V':
                arabic = 5;
                break;
            case 'X':
                arabic = 10;
                break;
            case 'L':
                arabic = 50;
                break;
            case 'C':
                arabic = 100;
                break;
            case 'D':
                arabic = 500;
                break;
            case 'M':
                arabic = 1000;
                break;
        }
        return arabic;
    }

}
