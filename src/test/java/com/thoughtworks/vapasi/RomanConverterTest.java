package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class RomanConverterTest {

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    //    Exception e = new IllegalArgumentException();
    }

    @Test
    void shouldConvertI() {
        assertEquals(1, romanConvertor.convertRomanToArabicNumber("I"));
    }

    @Test
    void shouldConvertII() {
        assertEquals(2, romanConvertor.convertRomanToArabicNumber("II"));
    }

    @Test
    void shouldConvertIII() {
        assertEquals(3, romanConvertor.convertRomanToArabicNumber("III"));

    }

    @Test
    void shouldConvertIV() {
        assertEquals(4, romanConvertor.convertRomanToArabicNumber("IV"));

    }

    @Test
    void shouldConvertV() {
        assertEquals(5, romanConvertor.convertRomanToArabicNumber("V"));

    }

    @Test
    void shouldConvertVI() {
        assertEquals(6, romanConvertor.convertRomanToArabicNumber("VI"));

    }

    @Test
    void shouldConvertVII() {
        assertEquals(7, romanConvertor.convertRomanToArabicNumber("VII"));

    }

    @Test
    void shouldConvertIX() {
        assertEquals(9, romanConvertor.convertRomanToArabicNumber("IX"));

    }

    @Test
    void shouldConvertX() {
        assertEquals(10, romanConvertor.convertRomanToArabicNumber("X"));

    }

    @Test
    void shouldConvertXXXVI() {
        assertEquals(36, romanConvertor.convertRomanToArabicNumber("XXXVI"));
    }

    @Test
    void shouldConvertMMXII() {
        assertEquals(2012, romanConvertor.convertRomanToArabicNumber("MMXII"));
    }

    @Test
    void shouldConvertMCMXCVI() {
        assertEquals(1996, romanConvertor.convertRomanToArabicNumber("MCMXCVI"));

    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        assertEquals(0, romanConvertor.convertRomanToArabicNumber("WW"));

    }
}